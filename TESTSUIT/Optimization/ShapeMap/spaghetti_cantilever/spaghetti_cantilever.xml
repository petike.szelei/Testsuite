<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
    
    <documentation>
    <title> feature mapping using linear spline ("spaghetti") with p-norm for feature aggregation
    </title>
    <authors>
      <author>Jannis Greifenstein</author>
    </authors>
    <date>2022-06-01</date>
    <keywords>
      <keyword>optimization</keyword>
    </keywords>
    <references>
    </references>
    <isVerified>no</isVerified>
        <description> cantilever optimization using generic anisotropic material </description>
  </documentation>
  
  <fileFormats>
    <output>
      <hdf5/>
      <info/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane">
    <regionList>
      <region material="weak" name="mech"/>
    </regionList>
    <nodeList>
      <nodes name="load">
        <coord x="1.0" y="0.0"/>
      </nodes>
      <nodes name="fix1">
        <coord x="0.0" y="1.0"/>
      </nodes>
      <nodes name="fix2">
        <coord x="0.0" y="0.0"/>
      </nodes>
    </nodeList>
  </domain>


  <sequenceStep index="1">
    <analysis>
      <static/>
    </analysis>

    <pdeList>
      <mechanic subType="planeStress">
        <regionList>
          <region name="mech"/>
        </regionList>

        <bcsAndLoads>
           <fix name="west"> 
              <comp dof="x"/> <comp dof="y"/> 
           </fix>
           <force name="load" >
             <comp dof="y" value="-1"/>
           </force>
           
        </bcsAndLoads>
        <storeResults>
          <elemResult type="mechPseudoDensity">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_1">
            <allRegions/>
          </elemResult>
          <elemResult type="optResult_2">
            <allRegions/>
          </elemResult>
          <elemResult type="mechTensor">
            <allRegions/>
          </elemResult>
          <nodeResult type="mechDisplacement">
            <allRegions/>
          </nodeResult>
        </storeResults>
      </mechanic>
    </pdeList>
    
<!--     <linearSystems> -->
<!--       <system> -->
<!--         <solverList> -->
<!--           <cholmod/> -->
<!--         </solverList> -->
<!--       </system> -->
<!--     </linearSystems> -->
  </sequenceStep>
    
  <optimization>
    <costFunction type="compliance" task="minimize" sequence="1" linear="false">
      <stopping queue="55" value="0.0001" type="relativeCostChange" maxHours="48"/>
    </costFunction>
   
    <optimizer type="snopt" maxIterations="1">
      <snopt>
        <option name="major_optimality_tolerance" type="real" value="1e-8"/>
        <option type="integer" name="verify_level" value="-1"/>
      </snopt>
    </optimizer>
    
    <ersatzMaterial region="mech" material="mechanic" method="spaghettiParamMat">
      <spaghettiParamMat combine="p-norm" boundary="poly" transition=".05" radius=".25">
        <python file="spaghetti.py" path="cfs:share:python" >
          <!-- silent disables command line output from pythons-->
          <option key="silent" value="1"/>
          <option key="order" value="3"/>
          <option key="p" value="8"/>
        </python> 
        <noodle segments="2">
          <node dof="x" initial="0." upper="1" lower="0" tip="start"/>
          <node dof="y" initial="0.0626311" upper="1" lower="0"  tip="start"/>
          <node dof="x" initial="1" upper="1" lower="0"  tip="end"/>
          <node dof="y" initial="0.0546454" upper="1" lower="0" tip="end"/>
          <profile fixed=".17"/>
          <normal initial="0." lower="-0.2" upper="0.2"/>
        </noodle>

        <noodle segments="2">
          <node dof="x" initial="0.0156926" upper="1" lower="0" tip="start"/>
          <node dof="y" initial="0.974114" upper="1" lower="0"  tip="start"/>
          <node dof="x" initial="1" upper="1" lower="0"  tip="end"/>
          <node dof="y" initial="0.077333" upper="1" lower="0" tip="end"/>
          <profile fixed=".17"/>
          <normal initial="0.0" lower="-0.2" upper="0.2"/>
        </noodle>
        <designMaterial type="density-times-rotated-2dtensor" bias="true">
          <param name="mech_11" value="1"/>
          <param name="mech_22" value="0.1"/>
          <param name="mech_33" value="0.05"/>
          <param name="mech_23" value="0"/>
          <param name="mech_13" value="0"/>
          <param name="mech_12" value="0.12"/>
        </designMaterial>
      </spaghettiParamMat>
    
      <design name="density" initial=".5" physical_lower="1e-3" upper="1.0"/>
      <design name="rotAngle" initial="0.2" lower="-6" upper="6.283185307179586"/>

      <transferFunction type="identity" application="mech" design="density" />
      <transferFunction type="identity" application="mech" design="rotAngle"/>
      
       <result value="design" id="optResult_1" design="density"/>
       <result value="design" id="optResult_2" design="rotAngle"/>
      <export save="all" write="iteration"/>
      
    </ersatzMaterial>
    <commit mode="forward" stride="1"/>
  </optimization>
</cfsSimulation>
