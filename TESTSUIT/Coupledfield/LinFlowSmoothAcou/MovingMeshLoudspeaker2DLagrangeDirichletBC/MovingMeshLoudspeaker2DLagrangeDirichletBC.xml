<?xml version="1.0" encoding="UTF-8"?>
<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.cfs++.org/simulation">
  
  <documentation>
    <title>updated geometry LinFlow</title>
    <authors>
      <author>fspoerk</author>
    </authors>
    <date>2022-07-19</date>
    <keywords>
      <keyword>flow</keyword>
      <keyword>smooth</keyword>
      <keyword>output</keyword>
    </keywords>
    <references> Non Just funcionality test </references>
    <isVerified>no</isVerified>
    <description>
   	  This test consists of a channel with three sections. A LinFlow section is coupled with an acoustic one which adjoins the PML (perfectly matched layer). The geometry is updated via smoothPDE. The velocity and gridvelocity are assigned to the first part of the Linflow section as boundary condition. This results in an oscillation and wave in the channel. Therefore, the geometry of the testcase stands in analogy to loudspeakers. The only difference to the original testcase is that we use Lagrange multiplier based BCs in order to test these BCs on moving domains.
    </description>
  </documentation>
  
  <fileFormats>
    <input>
      <cdb fileName="Channel_mesh.cdb"/>
      <!--<hdf5 fileName="MovingMeshLoudspeaker2D.h5ref" readEntities="Channel_LF_updated;Channel_LF;Channel_Acou;PML;Excite;LF_Bot_updated;LF_Top_updated;LF_Fix;LF_Bot;LF_Top;IF_M;IF_S"/>-->
    </input>
    <output>
      <hdf5 id="h5"/>
      <text id="txt"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>
  
  <domain geometryType="plane">
    <regionList>
      <region name="Channel_LF_updated" material="FluidMat"/>
      <region name="Channel_LF" material="FluidMat"/>
      <region name="Channel_Acou" material="FluidMat"/>
      <region name="PML" material="FluidMat"/>
    </regionList>
    
    <surfRegionList>
      <surfRegion name="Excite"/>
      <surfRegion name="LF_Bot_updated"/>
      <surfRegion name="LF_Top_updated"/>
      <surfRegion name="LF_Fix"/>
      <surfRegion name="LF_Bot"/>
      <surfRegion name="LF_Top"/>
      <surfRegion name="IF_M"/>
      <surfRegion name="IF_S"/>
    </surfRegionList> 
    
    <ncInterfaceList>
      <ncInterface name="NCI" masterSide="IF_M" slaveSide="IF_S"/>
    </ncInterfaceList>
    
    <nodeList>
      <nodes name="S1">
        <coord x="1" y="0" z="0"/>
      </nodes>
    </nodeList>
  </domain>
  
  <fePolynomialList>
    <!-- Set second order polynomial for velocity -->
    <Lagrange id="orderVel">
      <isoOrder>2</isoOrder> 
    </Lagrange>
    
    <!-- Set first order polynomial for pressure -->
    <Lagrange id="orderPres">
       <isoOrder>1</isoOrder> 
    </Lagrange>
  </fePolynomialList>

  <integrationSchemeList>
    <scheme id="integVel">
      <method>Gauss</method>
      <order>6</order>
      <mode>absolute</mode>
    </scheme>
    
    <scheme id="integPres">
      <method>Gauss</method>
      <order>4</order>
      <mode>absolute</mode>
    </scheme>    
  </integrationSchemeList>  
  
  <sequenceStep>
    <analysis>
      <transient>
        <numSteps>25</numSteps>
        <deltaT>5e-5</deltaT>
      </transient>
    </analysis>
    
    <pdeList>
      <smooth subType="planeStrain">
        <regionList>
          <region name="Channel_LF_updated"/>
        </regionList>
        
        <bcsAndLoads>
          <fix name="LF_Top_updated">
            <comp dof="y"/>
          </fix>
          <fix name="LF_Bot_updated">
            <comp dof="y"/>
          </fix>
          <fix name="LF_Fix">
            <comp dof="x"/>
            <comp dof="y"/>
          </fix>
          <displacement name="Excite">
            <comp dof="x" value="100*1e-3/(2*pi)*sin(2*pi*500*t)*(1-exp(-(t/(1e-4))^1))"/>
          </displacement>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="smoothDisplacement">
            <allRegions/>
          </nodeResult>    
        </storeResults>          
      </smooth>   
      
      <fluidMechLin formulation="compressible" presPolyId="orderPres" velPolyId="orderVel" presIntegId="integPres" velIntegId="integVel">
        <regionList>
          <region name="Channel_LF_updated" movingMeshId="moveGridID"/>
          <region name="Channel_LF"/>
        </regionList>
        
        <movingMeshList>
          <movingMesh name="moveGridID">
            <coupling pdeName="smooth">
              <quantity name="smoothVelocity"/>
            </coupling>
          </movingMesh>
        </movingMeshList>
               
        <bcsAndLoads>
          <velocityConstraint name="Excite" volumeRegion="Channel_LF_updated">
            <noSlip/>
          </velocityConstraint>
          <scalarVelocityConstraint name="LF_Top_updated" volumeRegion="Channel_LF_updated">
            <noPenetration/>
          </scalarVelocityConstraint>
          <scalarVelocityConstraint name="LF_Bot_updated" volumeRegion="Channel_LF_updated">
            <noPenetration/>
          </scalarVelocityConstraint>
          <scalarVelocityConstraint name="LF_Top" volumeRegion="Channel_LF">
            <noPenetration/>
          </scalarVelocityConstraint>
          <scalarVelocityConstraint name="LF_Bot" volumeRegion="Channel_LF">
            <noPenetration/>
          </scalarVelocityConstraint>
        </bcsAndLoads>
        
        <storeResults>
          <nodeResult type="fluidMechVelocity">
            <allRegions outputIds="h5"/>         
          </nodeResult>
          <nodeResult type="fluidMechPressure">
            <allRegions outputIds="h5"/>         
          </nodeResult>
        </storeResults>
      </fluidMechLin>
      
      <acoustic formulation="acouPotential">
        <regionList>
          <region name="Channel_Acou" polyId="orderPres"/>
          <region name="PML" dampingId="myPML" polyId="orderPres"/>
        </regionList>
        
        <dampingList>
          <pml id="myPML">
            <type>inverseDist</type>
            <dampFactor>1.0</dampFactor>
          </pml>
        </dampingList>
        
        <storeResults>
          <nodeResult type="acouPotential">
            <allRegions outputIds="h5"/>
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <nodeResult type="acouPotentialD1">
            <allRegions outputIds="h5"/>
            <nodeList>
              <nodes name="S1" outputIds="txt"/>
            </nodeList>
          </nodeResult>
          <elemResult type="acouVelocity">
            <allRegions outputIds="h5"/>
          </elemResult>
        </storeResults>
      </acoustic>
    </pdeList>
    
    <couplingList>
      <direct>
        <linFlowAcouDirect>
          <ncInterfaceList>
            <ncInterface name="NCI"/>
          </ncInterfaceList>
        </linFlowAcouDirect>
      </direct>
      <iterative PDEorder="mechanic;smooth">
        <convergence logging="yes" maxNumIters="5" stopOnDivergence="no">
          <quantity name="smoothDisplacement" value="1e-3" normType="rel"/>
          <quantity name="smoothVelocity" value="1e-3" normType="rel"/>
          <quantity name="fluidMechVelocity" value="1e-3" normType="rel"/>
          <quantity name="fluidMechPressure" value="1e-3" normType="rel"/>
        </convergence>
        <geometryUpdate>
          <region name="Channel_LF_updated"/>
        </geometryUpdate>
      </iterative>
    </couplingList>
    
    <linearSystems>
      <system>
        <solutionStrategy>
          <standard>
            <matrix storage="sparseNonSym"/>
          </standard>
        </solutionStrategy>
        <solverList>
          <pardiso>
            <posDef>no</posDef>
            <symStruct>no</symStruct>
            <logging>yes</logging>
          </pardiso>
        </solverList>
      </system>
    </linearSystems>
  </sequenceStep>
</cfsSimulation>
