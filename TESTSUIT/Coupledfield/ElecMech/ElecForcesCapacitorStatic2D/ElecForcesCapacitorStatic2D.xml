<?xml version="1.0" encoding="UTF-8"?>

<cfsSimulation xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/master/CFS-Simulation/CFS.xsd"
    xmlns="http://www.cfs++.org/simulation">
    
    <documentation>
        <title>2D Static Analysis of a Plate Capacitor</title>
        <authors>
            <author>Georg Jank</author>
        </authors>
        <date>2020-11-02</date>
        <keywords>
            <keyword>electrostatic</keyword>
        </keywords>
        <references></references>
        <isVerified>yes</isVerified>
        <description>
            This simulation consists of two conducting plates. Between the plates
            there is an air gap. The lower plate is fixed and the upper plate is 
            attached to a spring (i.e. material with poisson number 0). A voltage
            is applied across the plates and the electrostatic forces moves the 
            upper plate towards the lower plate. The aim of this Testcases is to 
            test electrostatic forces in a 2D model.
        </description>
    </documentation>
    
    <fileFormats>
        <input>
            <cdb fileName="ElecForcesCapacitorStatic2D.cdb"/>
        </input>
        <output>
            <hdf5/>
            <!--<text/>-->
        </output>
        <materialData file="mat.xml"/>
    </fileFormats>
    
    <domain geometryType="plane">
        <regionList>
            <region name="S_air" material="air"></region>
            <region name="S_upper_electrode" material="solid"></region>
            <region name="S_lower_electrode" material="solid"></region>
            <region name="S_spring" material="spring"></region>
        </regionList>
        <surfRegionList>
            <surfRegion name="L_upper_electrode"/>
            <surfRegion name="L_lower_electrode"/>
        </surfRegionList>
    </domain>
    
    <fePolynomialList>
        <Lagrange id="Lagrange1">
            <isoOrder>1</isoOrder>
        </Lagrange>
        <Lagrange id="Lagrange2">
            <isoOrder>2</isoOrder>
        </Lagrange>
    </fePolynomialList>
    
    <sequenceStep index="1">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <electrostatic>
                <regionList>
                    <region name="S_air" polyId="Lagrange2"/>
                </regionList>
                <bcsAndLoads>
                    <potential name="L_upper_electrode" value="1"></potential>
                    <ground name="L_lower_electrode"></ground>
                </bcsAndLoads>
                <storeResults>
                    <elemResult type="elecFieldIntensity">
                        <allRegions/>
                    </elemResult>
                    <surfElemResult type="elecForceDensity">
                        <surfRegionList>
                            <surfRegion name="L_upper_electrode"/>
                        </surfRegionList>
                    </surfElemResult>
                    <surfElemResult type="elecChargeDensity">
                        <surfRegionList>
                            <surfRegion name="L_upper_electrode"/>
                        </surfRegionList>
                    </surfElemResult>    
                    <elemResult type="elecEnergyDensity">
                        <allRegions/>
                    </elemResult>
                </storeResults>
            </electrostatic>
        </pdeList>
    </sequenceStep>
    
    <sequenceStep index="2">
        <analysis>
            <static/>
        </analysis>
        <pdeList>
            <mechanic subType="planeStrain">
                <regionList>
                    <region name="S_upper_electrode" polyId="Lagrange2"/>
                    <region name="S_spring" polyId="Lagrange2"/>
                </regionList>
                <bcsAndLoads>
                    <traction name="L_upper_electrode"> 
                        <sequenceStep index="1">
                            <quantity name="elecForceDensity" pdeName="electrostatic"/>
                            <timeFreqMapping>
                                <constant step="1"/>
                            </timeFreqMapping>
                        </sequenceStep>
                    </traction>
                    <fix name="L_fix">
                        <comp dof="x"/>
                        <comp dof="y"/>
                    </fix>
                    <fix name="L_guide">
                        <comp dof="x"/>
                    </fix>
                </bcsAndLoads> 
                <storeResults>
                    <nodeResult type="mechDisplacement">
                        <allRegions/>
                        <!--<nodeList>
                            <nodes name="sensor"/>
                        </nodeList>-->
                    </nodeResult>
                </storeResults>
            </mechanic>
        </pdeList>
    </sequenceStep>
    
</cfsSimulation>
