<?xml version="1.0"?>

 <cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
   <documentation>
     <title>Conductive Plate with Coil Excitation</title>
     <authors>
       <author>hauck</author>
     </authors>
     <date>2013-05-03</date>
     <keywords>
       <keyword>multiSequence</keyword>
     </keywords>
     <references>n.a. </references>
     <isVerified>no</isVerified>
     <description>
       Proof of concept example for a two-step simulation
       of a Lorentz force problem, consisting of a coil and a
       conductive plate, surrounded by air.
       
       In a first simulation run (see Plate3DEdge-step1.xml), only
       a transient magnetic simulation is performed, where the
       allowPostProc-status is set.
      
       Within the second simulation (this file) in a first 
       multisequence step the Lorentz forces of the previous magnetic
       simulation are applied as force density on the RHS of the 
       mechanical PDE. The timestep size  is 10 times smaller
       compared to the one in the previous magnetic simulation.
       
       In a second step, no further excitation is considered, but 
       the structure remains vibrating. Here, the time step
       is much coarser compared to the first simulation step.
     </description>
   </documentation>
   
   <fileFormats>
     <input> 
       <mesh/>
       <!-- Load information of 1st simulation run -->
       <hdf5 fileName="results_hdf5/Plate3DEdge-step1.h5" id="input" gridId="pp"/>
     </input>
     <output>
       <hdf5/>
     </output>
     <materialData file="mat.xml" format="xml"/>
   </fileFormats>
   
   <domain geometryType="3d">
     <regionList>
       <region name="airV"  material="AIR"/>
       <region name="plateV"  material="alu"/>
       <region name="coilV1" material="AIR"/>
       <region name="coilV2" material="AIR"/>
       <region name="coreV" material="AIR"/>
     </regionList>
     <elemList>
       <elems name="bottom"/>
       <elems name="top"/>
       <elems name="surfxOuter"/>
       <elems name="surfyOuter"/>
       <elems name="surfxInner"/>
       <elems name="surfyInner"/>
     </elemList>
     <nodeList>
       <nodes name="fixMech"/>
     </nodeList>
   </domain>
   
   
   <!-- ===================== -->
   <!--  First Sequence Step  -->
   <!-- ===================== -->
   <sequenceStep index="1">
     <analysis>
       <transient>
         <numSteps>50</numSteps>
         <deltaT>1e-4</deltaT>
       </transient>
     </analysis>
     
     <pdeList>
       <mechanic subType="3d">
         <regionList>
           <region name="plateV"/>
         </regionList>
         <bcsAndLoads>
           <fix name="fixMech">
             <comp dof="x"/>
             <comp dof="y"/>
             <comp dof="z"/>
           </fix>
           
           <!-- Calculate the Lorentz force from the previous magnetic s
           simulation run and apply it as force term on the RHS.
           As the time disretisation in the magnetic simulation 
             is different (dt = 1e-3), we apply time interpolation. -->
               <forceDensity name="plateV">
                 <externalSimulation inputId="input" sequenceStep="1">
                   <quantity name="magForceLorentzDensity" pdeName="magneticEdge"/>
                   <timeFreqMapping>
                     <continuous interpolation="linear" />
                   </timeFreqMapping>
                 </externalSimulation>
               </forceDensity>
               
             </bcsAndLoads>
             <storeResults>
               <nodeResult type="mechDisplacement">
                 <allRegions/>
               </nodeResult>
               <nodeResult type="mechRhsLoad">
                 <allRegions/>
               </nodeResult>
               
             </storeResults>
           </mechanic>
         </pdeList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>

       </sequenceStep>
       
       <!-- ===================== -->
       <!--  First Sequence Step  -->
       <!-- ===================== -->
       <sequenceStep index="2">
         <analysis>
           <transient>
             <numSteps>140</numSteps>
             <deltaT>5e-4</deltaT>
           </transient>
         </analysis>
         
         <pdeList>
           <mechanic subType="3d">
             <regionList>
               <region name="plateV"/>
             </regionList>
             
             <!-- Use results of previous sequence step as
                  initial values for this transient step -->
             <initialValues>
               <initialState>
                 <sequenceStep index="1"/>
               </initialState>
             </initialValues>
             
             
             <bcsAndLoads>
               <fix name="fixMech">
                 <comp dof="x"/>
                 <comp dof="y"/>
                 <comp dof="z"/>
               </fix>
              
               </bcsAndLoads>
               <storeResults>
                 <nodeResult type="mechDisplacement">
                   <allRegions/>
                 </nodeResult>
                 <nodeResult type="mechRhsLoad">
                   <allRegions/>
                 </nodeResult>
                 
               </storeResults>
             </mechanic>
           </pdeList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>

         </sequenceStep>
       </cfsSimulation>
       
       