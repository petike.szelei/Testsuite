<?xml version="1.0"?>

<cfsSimulation xmlns="http://www.cfs++.org/simulation" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.cfs++.org/simulation http://cfs-doc.mdmt.tuwien.ac.at/xml/CFS-Simulation/CFS.xsd">
  <documentation>
    <title>Infinite 2D cylinder with rotated square-shaped PML (potential)</title>
    <authors>
      <author>hauck</author>
    </authors>
    <date>2012-12-19</date>
    <keywords>
      <keyword>acoustic</keyword>
      <keyword>pml</keyword>
    </keywords>
    <references>
      Infinite cylinder example of Kaltenbacher, "The Book", 2nd edition,
      p. 186      
    </references>
    <isVerified>yes</isVerified>
    <description>
      This examples models a 2D cross section of a vibrating cylinder,
      where we just model one quarter part, which is rotated by 
      30 degree.
      
      Here we utilize a rotated perfectly matched layer, which is defined
      w.r.t. to the rotated Cartesian coordinate system.W
      e calculate the analytical solution (result "analytical"), 
      as well as the  point-wise difference between the analytical solution and 
      the FE solution (result "diff").
      
      In this example the potential formulation is used.
    </description>
  </documentation>
  
  <fileFormats>
    <output> 
      <hdf5 id="1"/>
      <text id="2" fileCollect="timeFreq"/>
    </output>
    <materialData file="mat.xml" format="xml"/>
  </fileFormats>

  <domain geometryType="plane" printGridInfo="yes">
    
    <!-- Define some global model variables -->
    <variableList>
      <var name="k" value="2.0*pi*0.1"/> <!-- wave number -->
      <var name="R0" value="8"/>         <!-- radius of inner cylinder -->
      <var name="angle" value="30"/>     <!-- rotation angle in degree -->
    </variableList>
    
    <regionList>
      <region name="inner" material="air"/>
      <region name="pml" material="air"/>
    </regionList>

    <nodeList>
      <nodes name="excite"/>
    </nodeList>
    
    <coordSysList>
      <!-- Rotated Cartesian coordinate systen, used for rotated PML.
           Note: The coordinate directions get parsed by the MathParser -->
      <cartesian id="rot">
        <origin/>
        <xAxis x="cos(angle/180*pi)" y="sin(angle/180*pi)"/>
        <yAxis x="0" y="0"/>
      </cartesian>
    </coordSysList>
    
  </domain>

  <fePolynomialList>
    <Legendre id="hi">
      <isoOrder>2</isoOrder>
    </Legendre>
  </fePolynomialList>

  <!-- ============================= -->
  <!-- 1st sequence step:            -->
  <!-- Harmonic simulation with PML  -->
  <!-- ============================= -->
  <sequenceStep index="1">
    <analysis>
      <harmonic>
        <numFreq>    1   </numFreq>
        <startFreq>  0.1 </startFreq>
        <stopFreq>   0.1 </stopFreq>
        <sampling> linear </sampling>
      </harmonic>
    </analysis>
    
    <pdeList>
      <acoustic formulation="acouPotential">
        
        <regionList>
          <region name="inner" polyId="hi"/>
          <region name="pml" dampingId="p" polyId="hi"/>
        </regionList>

        <dampingList>
          
          <!-- Define perfectly matched layer in rotated coordinate systen -->
          <pml id="p">
            <coordSysId>rot</coordSysId>
            <type> inverseDist</type>
            <dampFactor> 1 </dampFactor>
          </pml>
        </dampingList>
        
        <bcsAndLoads>
          <pressure name="excite" value="1.0"/>
        </bcsAndLoads>
        
        <storeResults>
           <elemResult type="acouPressure" complexFormat="amplPhase"> 
            <regionList>
              <!-- Perform additional post-processing in the propagation region -->
              <region name="inner" postProcId="diff"/>
              <region name="pml" />
            </regionList>
          </elemResult>
          
          <nodeResult type="acouPotential">
            <allRegions/>
          </nodeResult>
          
          <nodeResult type="acouPotentialD1" complexFormat="realImag"> 
            <allRegions/>
          </nodeResult>
          
          <elemResult type="acouIntensity" complexFormat="realImag">
            <allRegions/>
          </elemResult>
          
          <elemResult type="acouVelocity" complexFormat="realImag">
            <allRegions/>
          </elemResult>
          
          <elemResult type="acouSpeedOfSound" complexFormat="realImag">
            <allRegions/>
          </elemResult>
          
          <elemResult type="density" complexFormat="realImag">
            <allRegions/>
          </elemResult>
          
	  <surfRegionResult type="acouPower">
	    <surfRegionList>
	      <surfRegion name="abc"/>
	      <surfRegion name="excite"/>
	    </surfRegionList>
	  </surfRegionResult>
          
          <surfElemResult type="acouNormalVelocity">
	    <surfRegionList>
	      <surfRegion name="abc"/>
	      <surfRegion name="excite"/>
	    </surfRegionList>
	  </surfElemResult>
	  
	  </storeResults>
      </acoustic>
    </pdeList>
    
    <postProcList>
      <!-- Here we calulcate the analytical solution as given in THE BOOK on
        p. 187, formula (5.172)
        
        p = p(R_0) * H_0^(2) (kr) / H_0^(2) (kR_0)
        H_0^(2) (x) = J(x) - i*Y(x)
        
        where H is the Hankel function, r the current position and R_0 the
        radius of the cylinder. J and Y are the cylindric bessel functions.
      -->
      <postProc id="anal">
        <function resultName="analytical" unit="Pa" outputIds="1,2" >
          <dof name="" realFunc="sqrt(besselCylJ(  sqrt(x^2+y^2)*k,0)^2 + 
            besselCylY(  sqrt((x^2) + (y^2))*k,0)^2) /
            sqrt(besselCylJ( k * R0,0)^2 + besselCylY(k*R0,0)^2)"/>
        </function>
      </postProc>

      <!-- Here we calculate the relative difference of the analytical solution  
           and the FE-solution -->
      <postProc id="diff">
        <function resultName="diff" unit="Pa" outputIds="1,2" postProcId="anal">
          <dof name="" realFunc="(sqrt(u_real^2+u_imag^2)-
            
            sqrt(besselCylJ(  sqrt(x^2+y^2)*k,0)^2 + 
            besselCylY(  sqrt((x^2) + (y^2))*k,0)^2) /
            sqrt(besselCylJ( k * R0,0)^2 + besselCylY(k*R0,0)^2))
            /
            (sqrt(besselCylJ(  sqrt(x^2+y^2)*k,0)^2 + 
            besselCylY(  sqrt((x^2) + (y^2))*k,0)^2) /
            sqrt(besselCylJ( k * R0,0)^2 + besselCylY(k*R0,0)^2))"/>
        </function>
      </postProc>
    </postProcList>

    <linearSystems>
      <system>
        <solverList>
          <directLU/>
        </solverList>
      </system>
    </linearSystems>

  </sequenceStep>
</cfsSimulation>
